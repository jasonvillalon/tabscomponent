module.exports = {
  "Name": "TabsComponent",
  "Description": "Tabs component",
  "Author": "Jason Villalon",
  "Repository": "git@bitbucket.org:jasonvillalon/tabscomponent.git",
  "AtomicDeps": [
    {
      "Name": "MDButton",
      "Description": "Mdbutton",
      "Author": "Jason Villalon",
      "Repository": "git@bitbucket.org:generator-react-component/mdbutton.git"
    },
    {
      "Name": "Layout",
      "Description": "Layout",
      "Author": "Jason Villalon",
      "Repository": "git@bitbucket.org:generator-react-component/ac-layout.git"
    },
    {
      "Name": "Card",
      "Description": "Card",
      "Author": "Jason Villalon",
      "Repository": "git@bitbucket.org:generator-react-component/card.git"
    },
    {
      "Name": "History",
      "Description": "History",
      "Repository": "https://github.com/jasonvillalon/history.git"
    },
    {
      "Name": "UpdateState",
      "Repository": "git@bitbucket.org:codehoodph/updatestate.git",
      "jspmDeps": {
        "npm:react-addons-update": "^0.14.7"
      }
    }
  ]
};

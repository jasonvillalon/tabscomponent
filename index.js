import React, {PropTypes} from "react"
import {History} from "react-router"

import dependencies from "./dependencies"

let {MDButton, Layout, Card, UpdateState} = dependencies

const Tabs = React.createClass({
  mixins: [History, Layout, UpdateState],
  getInitialState() {
    return {
      tabs: null,
      tabContent: null,
      selectedIndex: 0
    }
  },
  propTypes: {
    onTabSelected: PropTypes.func,
    children: PropTypes.node,
  },
  componentDidMount() {
    this.prepareComponent(this.props)
    setTimeout(() => {
      if (this.props.onTabSelected) {
        this.props.onTabSelected(this.props.children[0])
      }
    }, 100)
  },
  componentWillReceiveProps(nextProps) {
    this.prepareComponent(nextProps)
  },
  onClickTab(props, i) {
    let selectedIndex = i
    let indicatorStyle = {
      "left": `calc(${100 / this.props.children.length}%*${i})`,
      "width": `${100 / this.props.children.length}%`
    }
    let tabContent = this.props.children[i].props.children
    this.updateState({
      selectedIndex,
      indicatorStyle,
      tabContent
    })
    if (this.props.onTabSelected) {
      this.props.onTabSelected(this.props.children[i])
    }
  },
  select(i) {
    this.updateState({
      selectedIndex: i,
      tabContent: this.props.children[i].props.children,
      indicatorStyle: {
        "left": `calc(${100 / this.props.children.length}%*${i})`,
        "width": `${100 / this.props.children.length}%`
      }
    })
    if (this.props.onTabSelected) {
      this.props.onTabSelected(this.props.children[i])
    }
  },
  prepareComponent(props) {
    try {
      let tabs = []
      let tabContent
      let selectedIndex
      props.children.map((tab, index) => {
        let onClick = () => {
          if (tab.props.link) {
            dependencies.History.push({pathname: tab.props.link})
          } else {
            this.onClickTab(props, index)
          }
        }
        tabs.push(<MDButton disabled={tab.props.disabled} key={`tab_${index}`} onClick={onClick} primary>{tab.props.title}</MDButton>)
        if (tab.props.link && this.history.isActive(tab.props.link)) {
          tabContent = tab.props.children ? tab.props.children : this.props.children
          selectedIndex = index
        }
      })
      if (!tabContent) {
        tabContent = props.children[this.state.selectedIndex].props.children
        selectedIndex = this.state.selectedIndex
      }
      this.updateState({
        tabs: tabs,
        tabContent: tabContent,
        indicatorStyle: {
          "left": `calc(${100 / props.children.length}%*${selectedIndex})`,
          "width": `${100 / props.children.length}%`
        }
      })
    } catch (err) {
      return new Error(err)
    }
  },
  render() {
    return (
      <div {...this.flex()} className="md-tabs" {...this.props} {...this.layout("column")}>
        <div className="md-tab-buttons">
          {this.state.tabs}
        </div>
        <div className="indicator-container">
          <div className="indicator" style={this.state.indicatorStyle}></div>
        </div>
        <Card {...this.flex()} className="md-tab-content">
          {this.state.tabContent}
        </Card>
      </div>
    )
  }
})

const Tab = React.createClass({
  propTypes: {
    children: PropTypes.node
  },
  render() {
    return (
      this.props.children
    )
  }
})

export default {
  Tabs,
  Tab
}

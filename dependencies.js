import MDButton from "../MDButton/index"
import Layout from "../Layout/index"
import Card from "../Card/index"
import History from "../History/index"
import UpdateState from "../UpdateState/index"


export default {
  MDButton,
  Layout,
  Card,
  History,
  UpdateState
}
